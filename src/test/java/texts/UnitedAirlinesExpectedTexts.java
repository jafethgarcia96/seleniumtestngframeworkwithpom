package texts;

public class UnitedAirlinesExpectedTexts {

//UNITED AIRLINES HOME PAGE
    public static final String homePageFromInputBoxText = "Chicago, IL, US (ORD)";
    public static final String homePageToInputBoxText = "Miami, FL, US (MIA)";

//UNITED AIRLINES DEPARTURE PAGE
    public static final String departurePageHeaderText = "Depart: Chicago, IL, US to Miami, FL, US";


}

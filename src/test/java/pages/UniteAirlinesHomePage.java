package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class UniteAirlinesHomePage {

    public UniteAirlinesHomePage(WebDriver driver){
        PageFactory.initElements(driver, this);
    }

    //TESTCASE1
    @FindBy(xpath = "//a[contains(@id,'headerNav')]")
    public List<WebElement> mainMenuNavItems;
    //TESTCASE2
    @FindBy(xpath = "//ul[contains(@class,'3RNBj')]/descendant::li")
    public List<WebElement> bookTravelMenu;
    //TESTCASE3
    @FindBy(xpath = "//input[@type='radio' and contains(@aria-label,'search')]")
    public List<WebElement> roundTripOneWayButtons;

    @FindBy(xpath = "//input[@type='radio' and contains(@aria-label,'search')]//..")
    public List<WebElement> labelRoundTripOneWayButtons;
    //TESTCASE4
    @FindBy(xpath = "//input[contains(@class,'bookFlightForm')]//..")
    public List<WebElement> bookFlexibleCheckBoxes;
    @FindBy(xpath = "//input[contains(@class,'bookFlightForm')]")
    public List<WebElement> actualBookFlexibleCheckBoxes;
    //TESTCASE5
    @FindBy(xpath = "//input[@aria-owns='bookFlightOriginInput-menu']")
    public WebElement fromInputBox;

    @FindBy(id = "bookFlightDestinationInput")
    public WebElement toInputBox;

    @FindBy(id = "DepartDate")
    public WebElement departDatesInputBox;

    @FindBy(xpath = "//td[contains(@aria-label, 'Sunday, January 30, 2022')]")
    public WebElement requiredDate;

    @FindBy(xpath = "//button[contains(@class, 'passengerButton')]")
    public WebElement travelersSelector;

    @FindBy(xpath = "//button[@aria-label='Clear Dates']")
    public WebElement clearDate;

    @FindBy(xpath = "//button[@aria-label='Substract one Adult']")
    public WebElement addAdultPLusButton;

    @FindBy(id = "cabinType")
    public WebElement cabinDropdown;
    @FindBy(id = "cabinType_item-2")
    public WebElement requiredCabinButton;
    @FindBy(xpath = "//button[@type='submit']")
    public WebElement findFlightsButton;


}

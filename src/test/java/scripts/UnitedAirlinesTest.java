package scripts;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.UniteAirlinesHomePage;
import texts.UnitedAirlinesExpectedTexts;
import utilities.ConfigReader;
import utilities.Waiter;

import java.util.List;

public class UnitedAirlinesTest extends Base{

    @Test(testName = "Validate Main menu navigation items", priority = 1)
    public void validateMainMenuNavigationItems(){
        driver.get(ConfigReader.getProperty("unitedAirlinesURL"));

        List<WebElement> mainMenuNavItems = homePage.mainMenuNavItems;

        for (WebElement mainMenuNavItem : mainMenuNavItems) {
            Assert.assertTrue(mainMenuNavItem.isDisplayed(), mainMenuNavItem.getText() + " mainMenuNavItem is not displayed");
        }
    }

    @Test(testName = "Validate book travel menu navigation items", priority = 2)
    public void validateBookTravelMenuNavigationItems(){
      driver.get(ConfigReader.getProperty("unitedAirlinesURL"));

        List<WebElement> bookTravelMenuNavItems = homePage.bookTravelMenu;
        for (WebElement bookTravelMenuNavItem:bookTravelMenuNavItems) {
            Assert.assertTrue(bookTravelMenuNavItem.isDisplayed());
        }
    }

    @Test(testName = "Validate Round-trip and One-way radio buttons", priority = 3)
    public void validateRoundTripOneWayRadioButtons(){
      driver.get(ConfigReader.getProperty("unitedAirlinesURL"));

        for (int i = 0; i < homePage.roundTripOneWayButtons.size(); i++) {

            Assert.assertTrue(homePage.labelRoundTripOneWayButtons.get(i).isDisplayed(),homePage.labelRoundTripOneWayButtons.get(i).getText() +" not displayed." );
            Assert.assertTrue(homePage.roundTripOneWayButtons.get(i).isEnabled(), homePage.roundTripOneWayButtons.get(i).getAttribute("id") +" not enabled.");
        }
            Assert.assertTrue(homePage.roundTripOneWayButtons.get(0).isSelected(), homePage.roundTripOneWayButtons.get(0).getAttribute("id") +" not selected.");
            Assert.assertFalse(homePage.roundTripOneWayButtons.get(1).isSelected(), homePage.roundTripOneWayButtons.get(1).getAttribute("id") +" is selected.");
            homePage.roundTripOneWayButtons.get(1).click();
            Assert.assertFalse(homePage.roundTripOneWayButtons.get(0).isSelected(), homePage.roundTripOneWayButtons.get(0).getAttribute("id") +" is selected.");
            Assert.assertTrue(homePage.roundTripOneWayButtons.get(1).isSelected(), homePage.roundTripOneWayButtons.get(1).getAttribute("id") +" not selected.");
    }

    @Test(testName = "Validate Book with miles and Flexible dates checkboxes", priority = 4)
    public void validateBookFlexibleCheckBoxes(){
        driver.get(ConfigReader.getProperty("unitedAirlinesURL"));

        for (int i = 0; i < homePage.bookFlexibleCheckBoxes.size(); i++) {
            if (i==1) {
                homePage.bookFlexibleCheckBoxes.get(1).click();
                homePage.bookFlexibleCheckBoxes.get(0).click();
            }
            for (int j = 0; j < homePage.bookFlexibleCheckBoxes.size(); j++) {
                if (i==0){
                    Assert.assertTrue(homePage.bookFlexibleCheckBoxes.get(j).isDisplayed());
                    Assert.assertTrue(homePage.bookFlexibleCheckBoxes.get(j).isEnabled());
                    Assert.assertFalse(homePage.bookFlexibleCheckBoxes.get(j).isSelected());
                }
                if (i==1){
                    Waiter.pause(2);
                    Assert.assertTrue(homePage.actualBookFlexibleCheckBoxes.get(j).isSelected());
                    if (j==1){
                        homePage.bookFlexibleCheckBoxes.get(1).click();
                        homePage.bookFlexibleCheckBoxes.get(0).click();
                        Assert.assertFalse(homePage.bookFlexibleCheckBoxes.get(j).isSelected());
                        Assert.assertFalse(homePage.bookFlexibleCheckBoxes.get(0).isSelected());
                    }
                }
            }
        }

    }

    @Test(testName = "Validate Validate One-way ticket search results", priority = 5)
    public void validateOneWayTicketSearchResults(){
        driver.get(ConfigReader.getProperty("unitedAirlinesURL"));

        homePage.roundTripOneWayButtons.get(1).click();
        homePage.fromInputBox.sendKeys(UnitedAirlinesExpectedTexts.homePageFromInputBoxText);
        homePage.toInputBox.sendKeys(UnitedAirlinesExpectedTexts.homePageToInputBoxText);
        homePage.departDatesInputBox.click();
        homePage.clearDate.click();
        homePage.departDatesInputBox.sendKeys("Jan 30");
        homePage.travelersSelector.click();
        homePage.addAdultPLusButton.click();
        homePage.cabinDropdown.click();
        homePage.requiredCabinButton.click();
        homePage.findFlightsButton.click();

        Assert.assertEquals(departPage.departureHeaderTxt.getText(), "Depart: Chicago, IL, US to Miami, FL, US");
    }

}

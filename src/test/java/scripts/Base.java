package scripts;

import com.github.javafaker.Faker;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.asserts.SoftAssert;
import pages.UniteAirlinesHomePage;
import pages.UnitedAirlinesDepartPage;
import utilities.Driver;

public class Base {

    WebDriver driver;
    Faker faker;
    Actions actions;
    SoftAssert softAssert;
    UniteAirlinesHomePage homePage;
    UnitedAirlinesDepartPage departPage;

    @BeforeMethod
    public void setUp(){
        driver = Driver.getDriver();
        faker = new Faker();
        actions = new Actions(driver);
        homePage = new UniteAirlinesHomePage(driver);
        departPage = new UnitedAirlinesDepartPage(driver);

        softAssert = new SoftAssert();
    }

    @AfterMethod
    public void tearDown(){
        Driver.quitDriver();
    }
}